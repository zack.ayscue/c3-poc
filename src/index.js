require('dotenv').config();

const util = require('util');
const stream = require('stream');
const AWS = require('aws-sdk');
const chunker = require('stream-chunker');
const SharepointService = require('./sharepoint');
const authCallback = require('./authCallback');

const s3 = new AWS.S3();
const pipeline = util.promisify(stream.pipeline);

const SHAREPOINT_BASE_ADDRESS = `https://${process.env.SHAREPOINT_HOST}/sites/${process.env.SHAREPOINT_SITE_ID}/_api/v2.0`;

const main = async (key, bucket) => {  
  try {
    // Starting
    console.log('Starting Cloud Copy Cat...');
    const start = new Date();

    const keyParts = key.split('/');
    const fileName = keyParts[keyParts.length - 1];
    let folderPath = '';
    if (keyParts.length > 1) {
      folderPath = keyParts.slice(0, keyParts.length - 1).join('/');
    }

    const fragmentSize = 320 * 1024 * 10;
    const { ContentLength: fileSize } = await s3.headObject({
      Key: key,
      Bucket: bucket
    }).promise();
    const getObjectRes = s3.getObject({
      Key: key,
      Bucket: bucket
    });

    const chunkTransformStream = chunker(fragmentSize, {
      flush:true
    });

    const sharepoint = new SharepointService({
      baseAddress: SHAREPOINT_BASE_ADDRESS,
      authCallback
    });
    const sharepointWriteStream = await sharepoint.createUploadSession(
      folderPath,
      fileName,
      fragmentSize,
      fileSize
    );

    const s3ReadStream = getObjectRes.createReadStream();
    

    await pipeline(
      s3ReadStream,
      chunkTransformStream,
      sharepointWriteStream
    );

    // Finishing
    const end = new Date();
    const duration = end - start;
    console.log(`Finished - Duration: ${duration}ms`);
  } catch (e) {
    console.error(e);
  }
};

main('recipes.csv', 'cloudcopycat');