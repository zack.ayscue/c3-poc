const { Writable } = require('stream');
const http = require('http');
const https = require('https');
const { default: axios } = require('axios');
const axiosRetry = require('axios-retry');

class SharepointWriteStream extends Writable {
  constructor(
    httpClient,
    uploadUrl,
    fragmentSize,
    fileSize,
    options
  ) {
    super(options);
    this.httpClient = httpClient;
    this.uploadUrl = uploadUrl;
    this.fragmentSize = fragmentSize;
    this.fileSize = fileSize;
    this.writtenBytes = 0;
  }

  _write(chunk, encoding, callback) {
    const contentLength = Buffer.byteLength(chunk);
    const range = `bytes ${this.writtenBytes}-${(this.writtenBytes) + (contentLength - 1)}/${this.fileSize}`;
    this.httpClient.put(
      this.uploadUrl,
      chunk,
      {
        headers: {
          'Content-Type': 'application/octet-stream',
          'Content-Length': contentLength.toString(),
          'Content-Range': range
        }
      }
    ).then(() => {
      this.writtenBytes += contentLength;
      callback();
    }).catch((error) => {
      this.emit('error', error);
    });
  }
};

class SharepointService {
  constructor(options) {
    this.baseAddress = options.baseAddress;
    this.authCallback = options.authCallback;
  }

  async createUploadSession(
    folderPath,
    fileName,
    fragmentSize,
    fileSize,
    conflictBehavior = 'replace'
  ) {
    const token = await this.authCallback();
    const httpClient = axios.create({
      headers: {
        Authorization: token
      },
      httpAgent: new http.Agent({ keepAlive: true }),
      httpsAgent: new https.Agent({ keepAlive: true })
    });
    axiosRetry(httpClient, { retries: 3 });

    const { data: { uploadUrl } } = await httpClient.post(
      `${this.baseAddress}/drive/root:/${folderPath}/${fileName}:/createUploadSession`, 
      {
        item: {
          '@microsoft.graph.conflictBehavior': conflictBehavior
        }
      }
    );

    return new SharepointWriteStream(
      httpClient,
      uploadUrl,
      fragmentSize,
      fileSize
    );
  }
};

module.exports = SharepointService;
