const { URLSearchParams } = require('url');
const { default: axios } = require('axios');

const authCallback = async () => {
  const {
    url,
    resource,
    client_id,
    client_secret,
    grant_type
  } = JSON.parse(process.env.SHAREPOINT_CREDENTIALS);

  const params = new URLSearchParams({
    resource,
    client_id,
    client_secret,
    grant_type
  });
  const { data : { token_type: tokenType, access_token: accessToken} } = await axios.post(url, params.toString());
  const token = `${tokenType} ${accessToken}`;
  return token;
};

module.exports = authCallback;
